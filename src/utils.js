const axios = require('axios').default
const {
  zipObject, uniq, sortBy, compact,
} = require('lodash')
const { format } = require('date-fns')
const ru = require('date-fns/locale/ru')

const FIELDS = [
  'date', 'dayOfWeek', 'start', 'end', 'name',
  'lecturer', 'place', 'type', 'lecturerId',
]

async function fetchSchedule(group) {
  group = encodeURIComponent(group)
  const { data } = await axios.get(`https://mai.ru/education/schedule/data/${group}.txt`)

  return data
}

async function doGroupExists(group) {
  try {
    await fetchSchedule(group)
    return true
  } catch (e) {
    // TODO - обработка всех статусов
    console.log(e.response && e.response.status)
    return false
  }
}

function parseSchedule(textSchedule) {
  const uniqLines = uniq(textSchedule.split(/\n|\r/))
  const lines = uniqLines.map(line => line.split('\t').map(part => part.trim()))
  const objectData = lines.map(line => zipObject(FIELDS, line))

  return objectData
}

async function getSchedule(group) {
  const textSchedule = await fetchSchedule(group)
  return parseSchedule(textSchedule)
}

function findLessons(date, lessons) {
  const dateStr = format(date, 'dd.MM.yyyy', { locale: ru })
  const lessonsForDate = lessons.filter(lesson => lesson.date === dateStr)
  // console.log(lessonsForDate)
  return sortBy(lessonsForDate, lesson => Number(lesson.start.split(':').shift()))
}

function formatLessons(lessons) {
  return lessons.map((lesson) => {
    const {
      start, end, name, type, lecturer, place,
    } = lesson

    const lines = [
      `⌚ ${start} -- ${end}`,
      `📝 ${name}, ${type}`,
      lecturer && `👤 ${lecturer}`,
      place && `📍 ${place}`,
    ]

    return compact(lines).join('\n')
  }).join('\n\n')
}

async function getFormattedSchedule(date, group) {
  const lessons = await getSchedule(group)
  const lessonsForDate = findLessons(date, lessons)
  return formatLessons(lessonsForDate)
}

function formatDate(date) {
  return format(date, 'dd.MM.yyyy, EEEE', { locale: ru })
}

module.exports = {
  doGroupExists,
  getFormattedSchedule,
  formatDate,
}
