const { VK } = require('vk-io')
const { Keyboard } = require('vk-io')
const { isFunction } = require('lodash')

const vk = new VK({
  token: process.env.TOKEN,
})

const bot = vk.updates

const MAIN_KEYBOARD = Keyboard.keyboard([
  [
    Keyboard.textButton({
      label: 'Сегодня',
      payload: {
        command: 'today',
      },
      color: Keyboard.PRIMARY_COLOR,
    }),
    Keyboard.textButton({
      label: 'Завтра',
      payload: {
        command: 'tomorrow',
      },
      color: Keyboard.PRIMARY_COLOR,
    }),
  ],
  [
    Keyboard.textButton({
      label: 'Помощь',
      payload: {
        command: 'help',
      },
    }),
    Keyboard.textButton({
      label: 'Найти препода',
      payload: {
        command: 'lecturer',
      },
    }),
  ],
])

bot.on('message', (context, next) => {
  const { messagePayload } = context

  context.state.command = messagePayload && messagePayload.command
    ? messagePayload.command
    : null

  return next()
})

// Simple wrapper for commands
const hearCommand = (names, conditions, handle) => {
  if (!Array.isArray(names)) {
    names = [names]
  }

  if (!isFunction(handle)) {
    handle = conditions
    conditions = [...names]
  }

  if (!Array.isArray(conditions)) {
    conditions = [conditions]
  }

  bot.hear(
    [
      (text, { state }) => (
        names.includes(state.command)
      ),
      ...conditions,
    ],
    handle,
  )
}

hearCommand(['help', 'start'], ['помощь', 'help', 'start'], async (context) => {
  await context.send({
    message: 'Хелпа будет',
    keyboard: MAIN_KEYBOARD,
  })
})

bot.setHearFallbackHandler(async (context) => {
  await context.send('Команда не распознана. Напишите "помощь" или нажмите на кнопку.')
})


module.exports = { bot, hearCommand }
