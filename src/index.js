require('dotenv').config()
require('./schedule')
require('./registration')
const { bot } = require('./bot')

bot.start().catch(console.error)
