const { bot, hearCommand } = require('./bot')
const { parseRequest } = require('./schedule-utils')
const { getFormattedSchedule, formatDate } = require('./utils')
const { getUser } = require('./storage')

async function handleScheduleRequest(context, request) {
  const { group } = await getUser(context.senderId)

  try {
    const date = parseRequest(request)
    const schedule = await getFormattedSchedule(date, group)
    const dateStr = formatDate(date)

    return context.send((schedule && `Расписание на ${dateStr}\n\n${schedule}`) || 'Пар нет :)')
  } catch (e) {
    return context.send(e.message)
  }
}

hearCommand('today', async context => handleScheduleRequest(context, 'сегодня'))

hearCommand('tomorrow', async context => handleScheduleRequest(context, 'завтра'))

bot.hear(
  value => (
    value && value.split(' ').shift().match(/^р(асписание)?$/)
  ),
  async (context) => {
    const parts = context.text.split(' ').slice(1)

    if (parts.length === 0) {
      return context.send('Для команды "расписание" нужны параметры')
    }

    const request = parts.pop()

    return handleScheduleRequest(context, request)
  },
)
