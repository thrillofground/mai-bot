const {
  parse,
  addDays,
  isPast,
  addWeeks,
  addMonths,
  isValid,
  isToday,
  getDate,
  getDaysInMonth,
  setDate,
} = require('date-fns')

const ru = require('date-fns/locale/ru')

function isDateString(str) {
  return Boolean(str.match(/^\d{1,2}([.-]\d{1,2})?$/))
}

function normalizeRequest(request) {
  request = request.toLowerCase()

  return request
}

function parseRelativeRequest(request) {
  const today = new Date()

  if (request.match(/с(егодня)?/)) {
    return today
  }

  if (request.match(/з(автра)?/)) {
    return addDays(today, 1)
  }

  return today
}

function parseDayOfWeek(day) {
  const date = parse(
    day,
    'eeee',
    new Date(),
    { locale: ru, weekStartsOn: 1 },
  )

  if (!isValid(date)) {
    throw new Error(`Некорректный запрос: ${day}`)
  }

  if (isPast(date) && !isToday(date)) {
    return addWeeks(date, 1)
  }

  return date
}

function parseDateString(dateStr) {
  let date = new Date()

  const todayDate = getDate(date)
  const [day, month] = dateStr.split(/\.|-/).map(part => Number(part))

  if (day && !month && day < todayDate) {
    date = addMonths(date, 1)
  }

  if (getDaysInMonth(date) < day || day < 1) {
    throw new Error(`Некорректная дата: ${date}`)
  }

  return setDate(date, day)
}

function parseRequest(request) {
  request = normalizeRequest(request)

  if (request.match(/^(с(егодня)?)$|^(з(автра)?)$/g)) {
    return parseRelativeRequest(request)
  }

  if (request.match(/^[а-я]+$/)) {
    return parseDayOfWeek(request)
  }

  if (isDateString(request)) {
    return parseDateString(request)
  }

  throw new Error(`Некорректный запрос: ${request}`)
}

module.exports = {
  parseRequest,
  isDateString,
  normalizeRequest,
  parseRelativeRequest,
  parseDayOfWeek,
  parseDateString,
}
