const { hearCommand } = require('./bot')
const { createOrUpdateUser } = require('./storage')
const { doGroupExists } = require('./utils')


hearCommand('regByCode', [/^рег(истрация)?/], async (context) => {
  const userId = context.senderId
  const parts = context.text.split(' ')

  if (parts.length === 1) {
    return context.send('Отправьте код группы')
  }

  const group = parts.pop()
  const isValidGroup = await doGroupExists(group)

  if (isValidGroup) {
    createOrUpdateUser({ userId, group })
    return context.send(`Теперь вы можете получать расписание для группы ${group}`)
  }

  return context.send(`
    Мы не нашли расписание для группы ${group}.
    Проверьте правильность кода группы.
    Например, "Бки" должно быть именно в таком регистре,
    а в начале кода группы "М3О" "О" - это буква, а не ноль`)
})
