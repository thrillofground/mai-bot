const mongo = require('mongoose')

mongo.connect(
  'mongodb://localhost/maiBot',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  },
)

// const db = mongo.connection
const userSchema = new mongo.Schema({
  userId: { type: Number, index: true },
  group: String,
})

const User = mongo.model('User', userSchema)

async function getUser(userId) {
  return User.findOne({ userId }).exec()
}

async function createOrUpdateUser(userData) {
  const { userId, group } = userData
  const user = await getUser(userId)

  if (user) {
    user.group = group
    await user.save()
  } else {
    const newUser = new User(userData)
    await newUser.save()
  }
}

module.exports = {
  getUser,
  createOrUpdateUser,
}
