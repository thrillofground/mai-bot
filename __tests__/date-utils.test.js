const {
  isToday,
  isTomorrow,
  isFuture,
  format,
  isValid,
} = require('date-fns')

const ru = require('date-fns/locale/ru')

const {
  parseRequest,
  isDateString,
  normalizeRequest,
  parseRelativeRequest,
  parseDayOfWeek,
  parseDateString,
} = require('../src/schedule-utils')


describe('isDateString', () => {
  it('определяет строки, похожие на дату', () => {
    const strings = [
      '1', '01', '1.01', '11.12',
    ]

    strings.forEach((str) => {
      expect(isDateString(str)).toBe(true)
    })
  })

  it('определяет строки, не похожие на дату', () => {
    const strings = [
      '1.', '01asd',
      'афва', '11.12.20',
    ]

    strings.forEach((str) => {
      expect(isDateString(str)).toBe(false)
    })
  })
})

describe('normalizeRequest', () => {
  it('переводит строку в нижний регистр', () => {
    expect(normalizeRequest('МаМа123')).toStrictEqual('мама123')
  })
})

describe('parseRelativeRequest', () => {
  it('правильно парсит команды', () => {
    expect(isToday(parseRelativeRequest('с'))).toBe(true)
    expect(isToday(parseRelativeRequest('сегодня'))).toBe(true)
    expect(isTomorrow(parseRelativeRequest('з'))).toBe(true)
    expect(isTomorrow(parseRelativeRequest('завтра'))).toBe(true)
  })
})

describe('parseDayOfWeek', () => {
  const days = [
    'пн',
    'вт',
    'ср',
    'чт',
    'пт',
    'сб',
    'вс',
  ]

  const daysFull = [
    'понедельник',
    'вторник',
    'среда',
    'четверг',
    'пятница',
    'суббота',
    'воскресенье',
  ]

  it('дата парсится для дней, которые еще не прошли', () => {
    days.concat(daysFull).forEach((day) => {
      const date = parseDayOfWeek(day)
      expect(isFuture(date) || isToday(date)).toBe(true)
    })
  })

  it('день недели парсится верно', () => {
    days.forEach((day, i) => {
      const date = parseDayOfWeek(day)
      expect(format(date, 'eeee', { locale: ru })).toStrictEqual(daysFull[i])
    })

    daysFull.forEach((day) => {
      const date = parseDayOfWeek(day)
      expect(format(date, 'eeee', { locale: ru })).toStrictEqual(day)
    })
  })

  it('выкидывает ошибку на некорректную строку', () => {
    const incorrectReqs = [
      'понед',
      'привет',
      'завтра',
      'вторнек',
    ]

    incorrectReqs.forEach((req) => {
      expect(() => parseDayOfWeek(req)).toThrow('Некорректный запрос')
    })
  })
})

describe('parseDateString', () => {
  const dateStrings = [
    '16', '17', '10', '01', '1', '10.11', '02.01',
  ]

  it('различные даты парсятся', () => {
    dateStrings.forEach((str) => {
      expect(isValid(parseDateString(str))).toBe(true)
    })
  })
})

describe('parseRequest', () => {
  it('выбрасывает ошибку на некорректный запрос', () => {
    const incorrectReqs = [
      'привет',
      'панидельк',
      '[asd//asd123&&13{}',
      'скажи расписание',
      '34.11',
      '34',
      '11.11.11',
      'зв',
      '123',
      '.1',
      '11.',
    ]

    incorrectReqs.forEach((req) => {
      expect(() => parseRequest(req)).toThrow('Некорр')
    })
  })
})
